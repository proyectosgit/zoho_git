var report = require("../resources/lib_createReport");

var initialize = require("../modules/initialize_library.js");
var login = require("../modules/login_library.js");
var solution = require("../modules/createSolutions_library.js");
var app = require("../modules/createApp_library.js");
var formularios = require("../modules/createForm_library.js");
var home = require("../modules/home_library.js");
var elementos = require("../modules/elementosForm_library.js");

describe ('FUN_ATC_0030_formularioExitosoDesdeRegistroDeCuenta', function(){
  
  
  it ('initialize', async function(){
    browser.waitForAngularEnabled(false);
    initialize.resolveURL();
    browser.executeScript('window.scrollTo(0,500);');                //SCROLL

    expect (element(by.tagName('h2')).getText()).toBe('¿Qué es el desarrollo de aplicaciones?').then(
      async function(){
        console.log('Estás en la URL principal de zoho');     /////////////////////////////
        await element(by.linkText('COMENZAR DE FORMA GRATUITA')).click();         //COMENZAR DE FORMA GRATUITA           //https://www.zoho.com/es-xl/creator/signup.html
        browser.sleep(5000)
      },
      function(){
        console.log('NO Entraste a la URL de zoho');       ///////
      })
  })
     
  it ('login', async function(){
    expect (element(by.tagName('h3')).getText()).toBe('Comience su prueba gratis de 15 días.').then(
      async function(){
        console.log('Estás en la página de inicio de zoho');    ///////////////////////////
        element(by.id('name')).clear().click(); 
        element(by.id('name')).sendKeys('jorge');                            //NOMBRE
        element(by.id('email')).clear().click(); 
        element(by.id('email')).sendKeys('jorgejorch11111@gmail.com');	                                  //CORREO ELECTRONICO
        element(by.id('password')).clear().click(); 
        element(by.id('password')).sendKeys('shshsh123..');	                                //CONTRASEÑA
        element(by.css('.x_contactnumber')).clear().click(); 
        element(by.css('.x_contactnumber')).sendKeys('5552634178');	                //TELEFONO
        element(by.id('signup-termservice')).click();	                 
        element(by.id('signup-termservice')).click();		                   //CHECKBOX TERMINOS Y CONDICIONES
        element(by.id('signupbtn')).click();
        await element(by.id('signupbtn')).click();	                   	//REGISTRO GRATIS     //https://creator.zoho.com/userhome/adomani_12/admindashboard#/
        browser.sleep(20000)
      },
      function(){
        console.log('NO llegaste a la página de inicio de zoho');     ///////
    })
  })

  it ('createSolution', async function(){
    //expect(element(by.css('h3.text-center')).getText()).toContain('! You can start building now.').then(
    element(by.css('.zc-page-header-title zc-float-left')).isPresent().then( 
      async function(){
        console.log('Estás en la página de crear soluciones');  ///////////////////////////
        await element(by.id('buttonId')).click();                 //CREATE NEW APPLICATION - FROM SCRATCH - CREATE           //https://creator.zoho.com/userhome/adomani_12/admindashboard#/creator/scratch
        browser.sleep(10000)  
      },
      function(){
        console.log('NO llegaste a la página de crear soluciones');  ////////
    })   
  })                                                                     
       
  it ('createApp', async function(){   
    element(by.css('.zcheckbox__label')).isDisplayed().then(
      async function(){
        console.log('Estás en la página de nueva aplicación');    ////////////////////////////
        //bajar el zoom
        element(by.id('newAppName')).clear().click();
        element(by.id('newAppName')).sendKeys('app1');                          //APPLICATION NAME
        element(by.id('scratch-create-new')).click();
        await element(by.id('scratch-create-new')).click();                            //CREATE APPLICATION          //https://creator.zoho.com/appbuilder/cynthia.mosso2/app1/edit
        browser.sleep(5000) 
      },
      function(){
        console.log('NO llegaste a la página de nueva aplicación');   //////////
     })
  })   
  
  it ('finally', function(){
    report.createPathReport();
  })

  browser.sleep(3000) 

});  

