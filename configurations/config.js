var utils = require('../resources/lib_utils.js');
var indexator = utils.getIndex(0);
var date_ = utils.getIndex(1);

exports.config = {
    framework: 'jasmine',
    automationName: 'UiAutomator1',
    specs: ['../scripts/FUN_ATC_0010_formularioDesdeCuentaLimpia.js'],
    seleniumAddress: 'http://localhost:4723/wd/hub',
    capabilities: {
        browserName: 'chrome',
        platformName: 'Android',
        deviceName: 'LGM7008722529',
        chromeOptions: {
            w3c: false
        }
    },
    jasmineNodeOpts: { defaultTimeoutInterval: 300000 },
    
    
    params: {
        index: indexator,
        cellPhoneCapabilities:'Q6',
        functionality: 'aplicativo zoho',
        Navigator: 'chrome',
        browserVersion: '103.0.5672.162',
        date_: date_,
        purpose: 'Validar el proceso de crear aplicativo de la aplicación zoho',
        status: 'passed',
        description: "All Acceptance Criteria were validated successfully",
        testCaseName: 'FUN_ATC_0010_formularioDesdeCuentaLimpia'
    }
    


}

