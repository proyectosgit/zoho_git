var parameters = require("../data/parameters.json");
var report = require("../resources/lib_createReport.js");

exports.loginCuentaVacia = async function(){
    expect (element(by.tagName(parameters.start.validationElement.selector)).getText()).toBe(parameters.start.validationElement.validationText).then(
        async function(){
          console.log('2 página inicio');    
          report.getScreenShot("log in");   
          await element(by.xpath(parameters.start.validationElement.InputButtonStartSesion)).click();    //INICIAR SESION     //https://accounts.zoho.com/signin?service_language=es&servicename=ZohoCreator&signupurl=https://www.zoho.com/es-xl/creator/signup.html
          element(by.css(parameters.login.validationElement.selector)).isDisplayed().then(   
            async function(){
              console.log('3 página login');    
              element(by.id(parameters.login.inputTextLogin.selector)).clear().click();
              element(by.id(parameters.login.inputTextLogin.selector)).sendKeys(parameters.login.inputTextLogin.user); 
              report.getScreenShot("User input"); 
              await element(by.id(parameters.login.inputNext.selector)).click();
              console.log('NEXT US');   
              element(by.id(parameters.login.inputPassword.selector)).clear().click();
              element(by.id(parameters.login.inputPassword.selector)).sendKeys(parameters.login.inputPassword.password);  
              element(by.id(parameters.login.inputButtonLogin.selector)).click();
              report.getScreenShot("log in button");
              await element(by.id(parameters.login.inputButtonLogin.selector)).click();          //INICIAR SESION     //https://creator.zoho.com/userhome/cynthia.mosso10/admindashboard#/
              console.log('NEXT PSS');   
              browser.sleep(5000)
            },
            function(){
              console.log('NO estás en la página de login');
          }) 
        },
        function(){
          console.log('NO llegaste a la página de inicio de zoho');
      })
}


