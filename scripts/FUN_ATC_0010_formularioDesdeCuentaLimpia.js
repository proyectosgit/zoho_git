var report = require("../resources/lib_createReport");

var initialize = require("../modules/initialize_library.js");
var login = require("../modules/login_library.js");
var solution = require("../modules/createSolutions_library.js");
var app = require("../modules/createApp_library.js");
var formss = require("../modules/createForm_library.js");
var home = require("../modules/home_library.js");
var elementss = require("../modules/formElements_library.js");

describe ('formularioExitosoDesdeCuentaSinAplicaciones', function(){
  
  it ('abrir Pagina Zoho', async function(){
    initialize.resolveURL();                
    home.comienzoGratuito();
  })    


  it ('hacer login', async function(){
    login.loginCuentaVacia();
  })


  it ('crear solucion', async function(){
    solution.eligeDesarrollo();    
  })    


  it ('crear aplicacion', async function(){   
    app.nuevaApp();
  })   

  it ('finally', function(){
    report.createPathReport();
  })

}) 