var report = require("../resources/lib_createReport");

var initialize = require("../modules/initialize_library.js");
var login = require("../modules/login_library.js");
var solution = require("../modules/createSolutions_library.js");
var app = require("../modules/createApp_library.js");
var formularios = require("../modules/createForm_library.js");
var home = require("../modules/home_library.js");
var elementos = require("../modules/elementosForm_library.js");

describe ('FUNFormularioexitoso', function(){
  
  
  it ('initialize', async function(){
    browser.waitForAngularEnabled(false);
    initialize.resolveURL();
    browser.executeScript('window.scrollTo(0,500);');                //SCROLL

    expect (element(by.tagName('h2')).getText()).toBe('¿Qué es el desarrollo de aplicaciones?').then(
      async function(){
        console.log('Estás en la URL principal de zoho');
        await element(by.linkText('COMENZAR DE FORMA GRATUITA')).click();         //COMENZAR DE FORMA GRATUITA           //https://www.zoho.com/es-xl/creator/signup.html
        browser.sleep(5000)
      },
      function(){
        console.log('NO Entraste a la URL de zoho');
      })
  })
     
  it ('login', async function(){
    expect (element(by.tagName('h3')).getText()).toBe('Comience su prueba gratis de 15 días.').then(
      async function(){
        console.log('Estás en la página de inicio de zoho');    
        await element(by.xpath('/html/body/main/div/div/div[1]/span/a')).click();    //INICIAR SESION       //https://accounts.zoho.com/signin?service_language=es&servicename=ZohoCreator&signupurl=https://www.zoho.com/es-xl/creator/signup.html
        element(by.css('.icon-SmartQR')).isDisplayed().then(   
          async function(){
            console.log('Estás en la página de login');    
            element(by.id('login_id')).clear().click();
            element(by.id('login_id')).sendKeys('cyngmail.com');                //US
            element(by.id('nextbtn')).click();
            await element(by.id('nextbtn')).click();
            element(by.css('.errorlabel')).isPresent().then(       
              async function(){
                console.log('El usuario no existe');  
                element(by.xpath('//*[@id="login_id"]')).clear().click();
                await element(by.xpath('//*[@id="login_id"]')).sendKeys('cynthia1111111@gmail.com');
                await element(by.id('nextbtn')).click();
                //como agrego una bandera que me regrese al then?  
              },
              async function(){
                element(by.id('password')).clear().click();
                element(by.id('password')).sendKeys('shshsh123..');                           //PSS
                element(by.id('nextbtn')).click();
                await element(by.id('nextbtn')).click();                    //INICIAR SESION     //https://creator.zoho.com/userhome/cynthia.mosso8/admindashboard#/
                browser.sleep(40000) 
            })           
          },
          function(){
            console.log('NO estás en la página de login');
        }) 
      },
      function(){
        console.log('NO llegaste a la página de inicio de zoho');
    })
  })
  
  it ('finally', function(){
    report.createPathReport();
  })
  
    browser.sleep(3000) 

});  

